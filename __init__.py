r"""
.. module:: MTIpython.fluid
    :platform: Unix, Windows
    :synopsis: packages for fluid calculations

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>, Maarten in 't Veld <jm.intveld@ihcmti.com>,
    Edwin de Hoog <e.dehoog@ihcmti.com>
"""

from MTIpython.fluid import principal

__all__ = ['principal']
