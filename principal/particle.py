r"""
.. module:: MTIpython.fluid.principal.particle
    :platform: Unix, Windows
    :synopsis: packages with commonly used powder calculations

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>, Edwin de Hoog <e.dehoog@ihcmti.com>,
    Maarten in 't Veld <jm.intveld@ihcmti.com>
"""
from enum import IntEnum

from numpy import array, log10, zeros, ndarray, where, logical_and
from math import pi
from MTIpython.core.exception import DimensionalityError
from MTIpython.units.SI import u, isquantity
from MTIpython.core.functions import function_switch

__all__ = ['settling_velocity', 'pickup_velocity']


def dimensionless_particle_size_grace(d, rho_f, rho_s, mu):
    r"""
    Dimensionless particle size :math:`d^{*}` in in :math:`[-]`

    .. math::

        d^{*} = d \sqrt[3]{\frac{\rho_f(\rho_s-rho_f)g}{\mu^2}}

    Source :cite:`matousek2004dredge`

    Args:
        d: diameter :math:`d` of a solid particle in :math:`[M^{1}]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`

    Returns:
        Dimensionless particle size :math:`d^{*}` in in :math:`[-]`
    """

    @u.wraps(u.dimensionless, (u.m, u.kg / u.m ** 3, u.kg / u.m ** 3, u.Pa * u.s), strict=False)
    def func(d, rho_f, rho_s, mu):
        return d * ((rho_f * (rho_s - rho_f) * (1 * u.g_0).m) / mu ** 2) ** (1 / 3)

    return func(d, rho_f, rho_s, mu)


def dimensionless_settling_velocity_grace(v_ts, rho_f, rho_s, mu):
    r"""
    Dimensionless settling velocity :math:`v_{ts}^{*}` in in :math:`[-]`

    .. math::

        v_{ts}^{*} = v_{ts} \sqrt[3]{\frac{\rho_f^2}{\mu(\rho_s-rho_f)g}}

    Source :cite:`matousek2004dredge`

    Args:
        v_ts: settling velocity :math:`v_{ts}` of a solid particle in :math:`[M^{1} t^{-1}]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`

    Returns:
        Dimensionless settling velocity :math:`u_{ts}^{*}` in in :math:`[-]`
    """

    @u.wraps(u.dimensionless, (u.m / u.s, u.kg / u.m ** 3, u.kg / u.m ** 3, u.Pa * u.s), strict=False)
    def func(v_ts, rho_f, rho_s, mu):
        return v_ts * (rho_f ** 2 / (mu * (rho_s - rho_f) * u.g_0)) ** (1 / 3)

    return func(v_ts, rho_f, rho_s, mu)


def settling_velocity_grace(v_ts_star, rho_f, rho_s, mu):
    r"""
    Settling velocity :math:`v_{ts}` in in :math:`[M^{1} t^{-1}]`

    .. math::

        v_{ts} = \frac{v_{ts}^{*}}{\sqrt[3]{\frac{\rho_f^2}{\mu(\rho_s-rho_f)g}}}

    Source :cite:`matousek2004dredge`

    Args:
        v_ts_star: Dimensionless settling velocity :math:`u_{ts}^{*}` in in :math:`[-]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`

    Returns:
        Settling velocity :math:`v_{ts}` in in :math:`[M^{1} t^{-1}]`
    """

    @u.wraps(u.m / u.s, (u.dimensionless, u.kg / u.m ** 3, u.kg / u.m ** 3, u.Pa * u.s), strict=False)
    def func(v_ts_star, rho_f, rho_s, mu):
        return v_ts_star * (rho_f ** 2 / (mu * (rho_s - rho_f) * u.g_0)) ** (-1 / 3)

    return func(v_ts_star, rho_f, rho_s, mu)


def dimensionless_settling_velocity_grace_as_function_d(d_star: u.Quantity):
    r"""
    Dimensionless settling velocity :math:`v_{ts}^{*}` in in :math:`[-]` using a correlation as a function of :math:`d^*`

    .. math::

        \begin{cases}
          v_{ts}^* = \frac{{d^*}^2}{18} - 3.1234 \dot 10^{-4} {d^*}^5 + 1.6415 \dot 10^{-6} {d^*}^8 & d^* \leq 3.8 \\
          \log_{10}(v_{ts}^*) = -1.5446 + 2.9162 \log_{10}(d^*) - 1.0432 \log_{10}(d^*)^2 & 3.8 < d^* \leq 7.58 \\
          \log_{10}(v_{ts}^*) = -1.64758 + 2.94786 \log_{10}(d^*) - 1.0907 \log_{10}(d^*)^2 + 0.17129 \log_{10}(d^*)^2 & 7.58 < d^* \leq 227 \\
          \log_{10}(v_{ts}^*) = 5.1837 - 4.51034 \log_{10}(d^*) - 1.687 \log_{10}(d^*)^2 + 0.189135 \log_{10}(d^*)^3 & 227 < d^* \leq 3500 \\
        \end{cases}

    Source: :cite:`matousek2004dredge`

    Args:
        d_star: Dimensionless particle size :math:`d^{*}` in in :math:`[-]`

    Returns:
        Dimensionless settling velocity :math:`u_{ts}^{*}` in in :math:`[-]`
    """
    isqnt = False
    if isquantity(d_star):
        isqnt = True
        if d_star.dimensionality != u.dimensionless:
            raise DimensionalityError("Quantity should be dimensionless!")
        d_star = d_star.m

    isarray = True
    d_star_type = type(d_star)
    if d_star_type is not ndarray:
        isarray = False
        d_star = array(d_star)

    idx = [where(logical_and(0. <= d_star, d_star <= 3.8)),
           where(logical_and(3.8 < d_star, d_star <= 7.58)),
           where(logical_and(7.58 < d_star, d_star <= 227.)),
           where(logical_and(227. < d_star, d_star <= 3500.))]

    sliced_d_stars = [
        d_star[d_star < 0.],
        d_star[idx[0]],
        d_star[idx[1]],
        d_star[idx[2]],
        d_star[idx[3]],
        d_star[d_star > 3500.]
    ]
    if len(sliced_d_stars[0]) > 0:
        raise ValueError('d_star should be atleast 0!')
    if len(sliced_d_stars[-1]) > 0:
        raise ValueError('d_star should be  3500!')

    v_st_star = zeros(d_star.shape)
    v_st_star[idx[0]] = sliced_d_stars[1] ** 2 / 18. - 3.1234e-4 * sliced_d_stars[1] ** 5 + 1.6415e-6 ** 8
    v_st_star[idx[1]] = 10. ** (-1.5446 + 2.9162 * log10(sliced_d_stars[2]) - 1.0432 * log10(sliced_d_stars[2]) ** 2)
    v_st_star[idx[2]] = 10. ** (-1.64758 + 2.94786 * log10(sliced_d_stars[3]) - 1.0907 * log10(sliced_d_stars[3]) ** 2
                                + 0.17129 * log10(sliced_d_stars[3]) ** 3)
    v_st_star[idx[3]] = 10. ** (5.1837 - 4.5103 * log10(sliced_d_stars[4]) + 1.687 * log10(sliced_d_stars[4]) ** 2
                                - 0.189135 * log10(sliced_d_stars[4]) ** 3)

    if isarray:
        return v_st_star * u.dimensionless if isqnt else v_st_star
    else:
        return d_star_type(v_st_star) * u.dimensionless if isqnt else d_star_type(v_st_star)


def settling_velocity(**kwargs):
    r"""
    Calculate the settling velocity :math:`v_{ts}` in :math:`[L^{1} t^{1}]` of a particle in a fluid

    .. math::

       v_{ts} = \frac{v_{ts}^*}{\sqrt[3]{\frac{\rho_f^2}{\mu_f (\rho_s - \rho_f) g_0}}}

    Source: :cite:`matousek2004dredge`

    Args:
        d: diameter :math:`d` of a solid particle in :math:`[M^{1}]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        medium: Liquid in which the particles resides
        granular: Granular material

    Returns:
        Calculate the settling velocity :math:`v_{ts}` in :math:`[L^{1} t^{1}]`
    """
    if 'medium' in kwargs.keys():
        medium = kwargs.pop('medium')
        rho_f = medium.rho
        mu = medium.mu
    if 'granular' in kwargs.keys():
        granular = kwargs.pop('granular')
        d = granular.PSD.size
        rho_s = granular.basematerial.rho
    d_star = dimensionless_particle_size_grace(d=d, rho_f=rho_f, rho_s=rho_s, mu=mu)
    v_ts_star = dimensionless_settling_velocity_grace_as_function_d(d_star=d_star)
    return v_ts_star / (rho_f ** 2 / (mu * (rho_s - rho_f) * u.g_0)) ** (1 / 3)


def pickup_velocity(**kwargs):
    r"""
    Pickup velocity :math:`v_pu` in :math:`[L^{1} t^{-1}]`

    .. math::

       v_{pu} = \frac{2.62\nu^{\frac{13}{21}}D^{\frac{3}{21}}}{\mu^{\frac{8}{21}}}\left(\frac{\pi}{6}g_0(\rho_s-\rho_f)+\frac{1.302\times10^{-6}}{d^2}\right)^{\frac{8}{21}}

    Source: :cite:`haydenEffectParticleCharacteristics2003`


    Args:
        nu: kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`.
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        d: diameter :math:`d` of a solid particle in :math:`[M^{1}]`
        D: diameter :math:`D` of the pipe in :math:`[M^{1}]`
        medium: Liquid in which the particles resides
        granular: Granular material
        component: Pipe or duct

    Returns:
        Pickup velocity :math:`v_pu` in :math:`[L^{1} t^{-1}]`
    """
    if 'medium' in kwargs:
        medium = kwargs.pop('medium')
        if 'rho_f' not in kwargs: kwargs['rho_f'] = medium.rho
        if 'mu' not in kwargs: kwargs['mu'] = medium.mu
        if 'nu' not in kwargs: kwargs['nu'] = medium.nu
    if 'granular' in kwargs:
        granular = kwargs.pop('granular')
        if 'd' not in kwargs: kwargs['d'] = granular.PSD.size
        if 'rho_s' not in kwargs: kwargs['rho_s'] = granular.basematerial.rho
    if 'component' in kwargs:
        component = kwargs.pop('component')
        if 'D' not in kwargs: kwargs['D'] = component.D
    return function_switch(pickup_velocity,
                           **kwargs)


def pickup_velocity_hayden(nu, mu, rho_f, rho_s, d, D):
    r"""
    Pickup velocity :math:`v_pu` in :math:`[L^{1} t^{-1}]` as determined by Hayden et. al

    .. math::

       v_{pu} = \frac{2.62\nu^{\frac{13}{21}}D^{\frac{3}{21}}}{\mu^{\frac{8}{21}}}\left(\frac{\pi}{6}g_0(\rho_s-\rho_f)+\frac{1.302\times10^{-6}}{d^2}\right)^{\frac{8}{21}}

    Source: :cite:`haydenEffectParticleCharacteristics2003`

    Args:
        nu: kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`.
        mu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        rho_f: density :math:`\rho_f` of the fluid in :math:`[M^{1} L^{-3}]`
        rho_s: density :math:`\rho_s` of the solid in :math:`[M^{1} L^{-3}]`
        d: diameter :math:`d` of a solid particle in :math:`[M^{1}]`
        D: diameter :math:`D` of the pipe in :math:`[M^{1}]`

    Returns:
        Pickup velocity :math:`v_pu` in :math:`[L^{1} t^{-1}]`
    """

    @u.wraps(u.m / u.s, (u.m ** 2 / u.s, u.Pa * u.s, u.kg / u.m ** 3, u.kg / u.m ** 3, u.m, u.m, u.m / u.s ** 2),
             strict=False)
    def func_8(nu, mu, rho_f, rho_s, d, D, g):
        return (2.62 * nu ** (13 / 21) * D ** (3 / 21)) / (mu ** (8 / 21)) * (
                pi / 6 * g * (rho_s - rho_f) + 1.302e-6 / d ** 2) ** (8 / 21)

    return func_8(nu, mu, rho_f, rho_s, d, D, 1. * u.g_0)


def pickup_speed_tiny_particle(Ar, v_pu0):
    r"""
    A semi-emperical correlation developed for pickup velocity :math:`v_{pu}` of a layer of particles for sizes ranging
    from :math:`10` till :math:`1000 [\mu m]`.

    Given by:

    .. :math::

        `v_{pu} = (1.27 Ar^{-^1/_3} + 0.036Ar^{^1/_3} + 0.45)(0.7Ar^{^{-1}/_5}+1)v_{pu0}`

    Source :cite:`kalman2005pickup`

    Args:
        Ar: Archimedes number given in :math:`[-]`
        v_puo: Pickup speed for a large particle :math:`[L^{1} t^{-1}]`

    Returns:
        Pickup speed :math:`v_{pu}` tiny particle, given in  :math:`[L^{1} t^{-1}]`
    """
    return (1.27 * Ar ** (-1 / 3) + 0.036 * Ar ** (1 / 3) + 0.45) * (0.70 * Ar ** (-1 / 5) + 1.) * v_pu0
