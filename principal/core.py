r"""
.. module:: MTIpython.fluid.principal.principal
    :platform: Unix, Windows
    :synopsis: packages with commonly used fluid calculations

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>, Edwin de Hoog <e.dehoog@ihcmti.com>,
    Maarten in 't Veld <jm.intveld@ihcmti.com>
"""
from enum import IntEnum

from numpy import array, log10, vectorize, zeros
from scipy.optimize import root

from MTIpython.core.core import rgetattr, rhasattr
from MTIpython.core.functions import function_switch
from MTIpython.reference.bibliography import citation
from MTIpython.units.SI import arequantities, isiterable, u

__all__ = ['FlowRegime', 'Re', 'flowregime', 'velocity', 'friction_factor', 'darcy', 'Ar', 'mass_flow', 'bernoulli',
           'pressure_loss', 'kinetic_energy', 'potential_energy']


class FlowRegime(IntEnum):
    r"""
    Enumerate for the different flow regimes
    """
    LAMINAR = 2000
    TURBULENT = -1
    TRANSITIONAL = 4000


def Re(**kwargs):
    r"""
    Function which calculates the Reynolds number for a :class:`.Liquid`, :class:`.Bingham` or :class:`.Gas` moving 
    through, or around a :class:`.Component`.

    Reynolds number for Newtonian fluids or Bingham plastics :math:`Re` in :math:`[-]`. Calculated using:

    .. math::
       Re = \frac{\rho v L}{\mu}

    .. math::
       Re = \frac{v L}{\nu}

    or for Bingham plastic using:

    .. math::
       Re = \frac{\rho v d}{\eta \left(1 + \frac{\tau d}{6 \eta v}\right)}

    Args:
        component (:class:`.Component`): Component acting as container, or disturbance in a
         fluid
        v (:class:`~pint:.Quantity`): Average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        L (:class:`~pint:.Quantity`): Component length :math:`L` in :math:`[L^{1}]`
        d (:class:`~pint:.Quantity`): Component diameter :math:`D` in :math:`[L^{1}]`
        fluid (:class:`.Liquid` or :class:`.Gas`): fluid moving through or around a component
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        mu (:class:`~pint:.Quantity`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        nu (:class:`~pint:.Quantity`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
        tau (:class:`~pint:.Quantity`): Yield stress :math:`\tau` in :math:`[M^{1} L^{-1} t^{-2}]`
        eta (:class:`~pint:.Quantity`): Tangential viscosity :math:`\eta` in :math:`[M^{1} L^{-1} t^{-1}]`
        category (:class:`MTIpython.material.base.Category` or int): Fluid category (default)
         :attr:`MTIpython.material.base.Category.FLUID`

    Kwargs Combinations:
        * component
        * fluid, component
        * fluid, v, L
        * fluid, v, d, category
        * rho, mu, component, (optional) category
        * rho, mu, v, L,(optional) category
        * rho, eta, tau, v, d, category
        * nu, component, (optional) category
        * nu, v, L, (optional) category

    Returns:
        :class:`~pint:.Quantity`: Reynolds number for fluids :math:`Re` in :math:`[-]`

    Raises:
        AttributeError:
    """
    component = kwargs.pop('component', None)
    fluid = kwargs.pop('fluid', component.fluid if component else None)
    cat = kwargs.pop('category', fluid.category.value if fluid else 3)
    try:
        if cat == 5:
            if 'd' not in kwargs:
                kwargs['d'] = component.d
            if 'eta' not in kwargs:
                kwargs['eta'] = fluid.eta
            if 'tau' not in kwargs:
                kwargs['tau'] = fluid.tau
        else:
            if 'L' not in kwargs:
                kwargs['L'] = component.L
            if 'mu' not in kwargs:
                if 'nu' not in kwargs:
                    if fluid.nu:
                        kwargs['nu'] = fluid.nu
                    elif fluid.mu:
                        kwargs['mu'] = fluid.mu

        if 'v' not in kwargs:
            kwargs['v'] = component.v
        if 'rho' not in kwargs and ('mu' in kwargs or cat == 5):
            kwargs['rho'] = fluid.rho
    except AttributeError as e:
        e.args = e.args + (
            r'Either set a component or fluid with an attribute {}, or pass it as a keyword argument!'.format(
                e.args[0][35:]),)
        raise

    if fluid is not None and hasattr(fluid, '_Re'):
        return fluid._Re(**kwargs)
    else:
        return function_switch(Re_newton_from_dynamic_viscosity,
                               Re_newton_from_kinematic_viscosity,
                               Re_bingham,
                               **kwargs)


def Re_newton(**kwargs):
    r"""
    Function which calculates the Reynolds number for a liquid or a gas moving through, or around a component.

    Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`. Calculated using:

    .. math::
       Re = \frac{\rho v L}{\mu}

    .. math::
       Re = \frac{v L}{\nu}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        mu (:class:`~pint:.Quantity`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        nu (:class:`~pint:.Quantity`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
        v (:class:`~pint:.Quantity`): Average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        L (:class:`~pint:.Quantity`): Component length :math:`L` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`
    """
    return function_switch(Re_newton_from_dynamic_viscosity,
                           Re_newton_from_kinematic_viscosity,
                           **kwargs)


@citation(r"""@book{white_fluid_2011,
              title = {Fluid {Mechanics}},
              isbn = {978-0-07-352934-9},
              publisher = {McGraw Hill},
              author = {White, Frank M.},
              year = {2011}
              }""")
def Re_newton_from_kinematic_viscosity(v, L, nu):
    r"""
    Function which calculates the Reynolds number for a liquid or gas moving through, or around a component.

    Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`. Calculated using:

    .. math::
       Re = \frac{v L}{\nu}

    Args:
        v (:class:`~pint:.Quantity`): Average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        L (:class:`~pint:.Quantity`): Component length :math:`L` in :math:`[L^{1}]`
        nu (:class:`~pint:.Quantity`): Kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`

    Returns:
        :class:`~pint:.Quantity`: Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`
    """
    return v * L / nu


@citation(r"""@book{white_fluid_2011,
              title = {Fluid {Mechanics}},
              isbn = {978-0-07-352934-9},
              publisher = {McGraw Hill},
              author = {White, Frank M.},
              year = {2011}
              }""")
def Re_newton_from_dynamic_viscosity(v, L, rho, mu):
    r"""
    Function which calculates the Reynolds number for a liquid or a gas moving through, or around a component.

    Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`. Calculated using:

    .. math::
       Re = \frac{\rho v L}{\mu}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        mu (:class:`~pint:.Quantity`): Dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        v (:class:`~pint:.Quantity`): Average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        L (:class:`~pint:.Quantity`): Component length :math:`L` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Reynolds number for Newtonian fluids :math:`Re` in :math:`[-]`
    """
    return rho * v * L / mu


@citation(r"""@book{matousek_dredgepumps_2004,
              title = {Dredgepumps and slurry transport},
              author = {Matousek, V},
              year = {2004}
              }""")
def Re_bingham(d, v, eta, tau, rho):
    r"""
    Function which calculates the Reynolds number for a Bingham plastic moving through, or around a component.

    Reynolds number for Bingham plastics :math:`Re` in :math:`[-]`. Calculated using:

    .. math::
       Re = \frac{\rho v d}{\eta \left(1 + \frac{\tau d}{6 \eta v}\right)}

    Args:
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`
        eta (:class:`~pint:.Quantity`): Tangential viscosity :math:`\eta` in :math:`[M^{1} L^{-1} t^{-1}]`
        tau (:class:`~pint:.Quantity`): Yield stress :math:`\tau` in :math:`[M^{1} L^{-1} t^{-2}]`
        v (:class:`~pint:.Quantity`): Average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        d (:class:`~pint:.Quantity`): Component diameter :math:`D` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Reynolds number for Bingham plastics :math:`Re` in :math:`[-]`
    """
    return rho * v * d / (eta * (1 + (tau * d) / (6 * eta * v)))


def flowregime(Re):
    r"""
    Determine flow regime

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`

    Returns:
        :class:`.FlowRegime`: Flow regime of a fluid
    """

    def flowregime_magnitude(Re):
        if Re < FlowRegime.LAMINAR.value:
            return FlowRegime.LAMINAR
        elif Re < FlowRegime.TRANSITIONAL.value:
            return FlowRegime.TRANSITIONAL
        else:
            return FlowRegime.TURBULENT

    flowregime_vect = vectorize(flowregime_magnitude)
    fg = flowregime_vect([Re])
    return fg[0]


def velocity(**kwargs):
    r"""
    Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]` when it is flowing with a certain rate
    through a given cross-section.

    When the mass flow and density is known the velocity is calculated according:

    .. math::
       v = \frac{\dot{m}}{\rho A}

    when a volumetric flow is given the velocity is obtained with:

    .. math::
      v = \frac{Q}{A}

    Args:
        component (:class:`.Component`): Component acting as container for a fluid
        fluid (:class:`.Liquid`, :class:`.Bingham` or :class:`.Gas`): fluid moving through a component
        A (:class:`~pint:.Quantity`): Cross section :math:`A` in :math:`[L^{2}]`
        Q (:class:`~pint:.Quantity`): Volume flow :math:`Q` in :math:`[L^{3} t^{-1}]`
        m_dot (:class:`~pint:.Quantity`): Mass flow :math:`\dot{m}` in  :math:`[M^{1} t^{-1}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`

    Kwargs Combinations:
        * component
        * fluid, component
        * rho, component
        * A, component
        * Q, component
        * Q, A
        * rho, m_dot, component
        * rho, A, component
        * rho, m_dot, A

    Returns:
        :class:`~pint:.Quantity`: Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]`
    """
    component = kwargs.pop('component', None)
    fluid = kwargs.pop('fluid', component.fluid if component else None)
    try:
        if 'A' not in kwargs:
            kwargs['A'] = component.A
        if 'Q' not in kwargs:
            if 'rho' not in kwargs and 'm_dot' not in kwargs:
                kwargs['rho'] = fluid.rho
                kwargs['m_dot'] = component.m_dot
            else:
                kwargs['Q'] = component.Q
    except AttributeError as e:
        e.args = e.args + (
            r'Either set a component or fluid with an attribute {}, or pass it as a keyword argument!'.format(
                e.args[0][35:]),)
        raise
    return function_switch(velocity_from_mass_flow,
                           velocity_from_volumetric_flow,
                           **kwargs)


def velocity_from_volumetric_flow(Q, A):
    r"""
    Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]` when it is flowing with a certain rate
    through a given cross-section.

    When a volumetric flow is given the velocity is obtained with:

    .. math::
      v = \frac{Q}{A}

    Args:
        Q (:class:`~pint:.Quantity`): Volume flow :math:`Q` in :math:`[L^{3} t^{-1}]`
        A (:class:`~pint:.Quantity`): Cross section :math:`A` in :math:`[L^{2}]`

    Returns:
        :class:`~pint:.Quantity`: Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]`
    """
    return Q / A


def velocity_from_mass_flow(m_dot, rho, A):
    r"""
    Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]` when it is flowing with a certain rate
    through a given cross-section.

    When the mass flow and density is known the velocity is calculated according:

    .. math::
       v = \frac{\dot{m}}{\rho A}

    Args:
        A (:class:`~pint:.Quantity`): Cross section :math:`A` in :math:`[L^{2}]`
        m_dot (:class:`~pint:.Quantity`): Mass flow :math:`\dot{m}` in  :math:`[M^{1} t^{-1}]`
        rho (:class:`~pint:.Quantity`): Density :math:`\rho` in :math:`[M^{1} L^{-3}]`

    Returns:
        :class:`~pint:.Quantity`: Average velocity of a fluid :math:`v` in :math:`[L^{1} t^{-1}]`
    """
    return m_dot / (rho * A)


def friction_factor(Re, eqType='Haaland', **kwargs):
    r"""
    Calculates a friction factor :math:`f` in :math:`[-]` for a given flow. When the flow regime is laminar :math:`f` is
    calculated with:

    .. math::
       f=\frac{64}{Re}

    For turbulent flows the "Haaland equation" is used by default.

    .. math::
       \frac{1}{\sqrt {f}} = -1.8 \log \left[ \left( \frac{\varepsilon/D}{3.7} \right)^{1.11} + \frac{6.9}{\mathrm{Re}}
       \right]

    The "Swamee-Jain equation" can be used as an alternative

    .. math::
        \frac{1}{\sqrt {f}} = -2 \log \left(\frac{\varepsilon/D}{3.7} + \frac{5.74}{\mathrm{Re}^{0.9}}\right)

    The friction factor can also be determined by solving the "Colebrook White equation" iteratively.

    .. math::
       \frac{1}{f^{\frac{1}{2}}} = -2.0 \log\left(\frac{\frac{\epsilon}{D}}{3.7}+\frac{2.51}{Re \times f^{\frac{1}{2}}}
       \right)

    .. plot::

       from MTIpython.units.SI import u
       from MTIpython.fluid.principal.principal import haaland, colebrook_white, swamee_jain
       import numpy as np
       import matplotlib.pyplot as plt

       eps = 0.046 * u.mm
       D = [10., 500.] * u.mm
       Re = np.arange(4000., 10000., 200.) * u.dimensionless

       for d in D:
           f_haaland = haaland(eps, d, Re)
           f_colebrook_white = colebrook_white(eps, d, Re)
           f_swamee_jain = swamee_jain(eps, d, Re)

           plt.plot(Re, f_haaland, label='Haaland @ d={}'.format(d))
           plt.plot(Re, f_colebrook_white, label='Colebrook White @ d={}'.format(d))
           plt.plot(Re, f_swamee_jain, label='Swamee-Jain @ d={}'.format(d))
       plt.legend()
       plt.title('Friction factor equations compared')
       plt.xlabel('Re [-]')
       plt.ylabel('friction factor [-]')
       plt.show()

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`
        component (:class:`.Component`): Component acting as container which interacts with the fluid
        epsilon (:class:`~pint:.Quantity`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
        d (:class:`~pint:.Quantity`): Inlet and outlet diameter :math:`d` in :math:`[L^{1}]`
        eqType (str): Which equation to use for laminar calculation, acceptable values: "Haaland", "Swamee-Jain",
         "Colebrook-White" (default) "Haaland"

    Kwargs Combinations:
        * Re
        * Re, component
        * Re, d, component
        * Re, epsilon, component
        * Re, epsilon, d

    Returns:
        :class:`~pint:.Quantity`: Friction factor :math:`f` in :math:`[-]`

    Note:
        The :func:`.colebrook_white` function is a relative slow function, due to its iterative nature, it is advised
        to use

    Todo:
        Check how the solver works for an array of Reynolds numbers, covering, both laminar and turbulent.
    """
    if eqType == 'Haaland':
        func = haaland
    elif eqType == 'Swamee-Jain':
        func = swamee_jain
    elif eqType == 'Colebrook-White':
        func = colebrook_white
    else:
        raise AttributeError('Friction equation unknown!')

    component = kwargs.pop('component', None)
    laminair_args = False
    try:
        if 'd' not in kwargs and hasattr(component, 'd'):
            kwargs['d'] = component.d
        if 'epsilon' not in kwargs and rhasattr(component, 'material.epsilon'):
            kwargs['epsilon'] = rgetattr(component, 'material.epsilon')
    except AttributeError as e:
        laminair_args = True

    flow_regime = flowregime(Re)
    if isiterable(flow_regime):
        laminar = friction_factor_laminar(Re=Re[flow_regime == FlowRegime.LAMINAR])
        transitional = func(Re=Re[flow_regime == FlowRegime.TRANSITIONAL], **kwargs)
        turbulent = func(Re=Re[flow_regime == FlowRegime.TURBULENT], **kwargs)
        ff = zeros(Re.shape)
        ff[flow_regime == FlowRegime.LAMINAR] = laminar
        ff[flow_regime == FlowRegime.TRANSITIONAL] = transitional
        ff[flow_regime == FlowRegime.TURBULENT] = turbulent
        return ff
    else:
        if flow_regime == FlowRegime.LAMINAR:
            return friction_factor_laminar(Re=Re)
        else:
            if laminair_args:
                raise AttributeError('Attribute d and epsilon or component not set!')
            return func(Re=Re, **kwargs)


def colebrook_white(epsilon, d, Re):
    r"""
    Calculates a friction factor :math:`f` in :math:`[-]` for a given flow. Using the accepted design formula for
    turbulent friction, proposed by Colebrook as an interpolation formula.

    .. math::
       \frac{1}{f^{\frac{1}{2}}} = -2.0 \log\left(\frac{\frac{\epsilon}{D}}{3.7}+\frac{2.51}{Re \times f^{\frac{1}{2}}}
       \right)

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`
        epsilon (:class:`~pint:.Quantity`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
        d (:class:`~pint:.Quantity`): Inlet and outlet diameter :math:`d` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Friction factor :math:`f` in :math:`[-]`
    """
    if arequantities(epsilon, d, Re):
        epsilon = epsilon.to(u.mm).m
        d = d.to(u.m).m
        Re = Re.m

    if isiterable(Re):
        if len(Re) == 0:
            return

    def coolbrook_white_magnitude(epsilon, d, Re):
        def fr(x):
            return (
                    -2 * log10((2.51 / (Re * x ** 0.5)) + (epsilon / (3.71 * d))) - 1.0 / x ** 0.5)

        f = root(fr, array([0.02])).x[0]
        return f

    coolbrook_white_vect = vectorize(coolbrook_white_magnitude)
    ff = coolbrook_white_vect(epsilon, d, [Re])
    return ff[0]


def swamee_jain(epsilon, d, Re, ):
    r"""
    Calculates a friction factor :math:`f` in :math:`[-]` for a given flow with the Swamee–Jain equation. It is used to
    solve directly for the Darcy–Weisbach friction factor f for a full-flowing circular pipe. It is an approximation of
    the implicit Colebrook–White equation.

    .. math::
        \frac{1}{\sqrt {f}} = -2 \log \left(\frac{\varepsilon/D}{3.7} + \frac{5.74}{\mathrm{Re}^{0.9}}\right)

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`
        epsilon (:class:`~pint:.Quantity`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
        d (:class:`~pint:.Quantity`): Inlet and outlet diameter :math:`d` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Friction factor :math:`f` in :math:`[-]`
    """
    if arequantities(epsilon, d, Re):
        epsilon = epsilon.to(d.u).m
        d = d.m
        Re = Re.m
    return 1 / (-2.0 * log10(((epsilon / d) / 3.7) + (5.74 / Re ** 0.9))) ** 2


def haaland(epsilon, d, Re):
    r"""
    Calculates a friction factor :math:`f` in :math:`[-]` for a given flow using the Haaland equation. Which was
    proposed in 1983 by Professor S.E. Haaland of the Norwegian Institute of Technology. It is used to solve directly
    for the Darcy–Weisbach friction factor :math:`f`  for a full-flowing circular pipe. It is an approximation of the
    implicit Colebrook–White equation, but the discrepancy from experimental data is well within the accuracy of the
    data.

    .. math::
       \frac{1}{\sqrt {f}} = -1.8 \log \left[ \left( \frac{\varepsilon/D}{3.7} \right)^{1.11} + \frac{6.9}{\mathrm{Re}}
       \right]

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`
        epsilon (:class:`~pint:.Quantity`): Surface roughness :math:`\varepsilon` in :math:`[L^{1}]`
        d (:class:`~pint:.Quantity`): Inlet and outlet diameter :math:`d` in :math:`[L^{1}]`

    Returns:
        :class:`~pint:.Quantity`: Friction factor :math:`f` in :math:`[-]`
    """
    if arequantities(epsilon, d, Re):
        epsilon = epsilon.to(d.u).m
        d = d.m
        Re = Re.m
    return 1 / (-1.8 * log10(((epsilon / d) / 3.7) ** 1.11 + (6.9 / Re))) ** 2


def friction_factor_laminar(Re):
    r"""
    Calculates a friction factor :math:`f` in :math:`[-]` for a given laminar flow.

    .. math::
       f=\frac{64}{Re}

    Args:
        Re (:class:`~pint:.Quantity`): Reynolds number :math:`re` in :math:`[-]` :func:`.Re`

    Returns:
        :class:`~pint:.Quantity`: Friction factor :math:`f` in :math:`[-]`
"""
    return 64 / Re


def darcy(**kwargs):
    r"""
    Calculate the headloss :math:`h_l` loss in :math:`[L^{1}]` or pressureloss :math:`\Delta p` in
    :math:`[M^{1} L^{-1} t^{-2}]` using Darcy's equation:

    .. math::

       h_l = f \frac{l}{d} \frac{v^2}{2g}

    or

    .. math::

       \Delta p = f \frac{l}{d} \frac{v^2 \rho}{2}

    Args:
        f: darcy-weisbach friction factor :math:`f` in :math:`[-]`
        l: length :math:`l` of a pipe in :math:`[L^{1}]`
        D: diameter :math:`d` in :math:`[L^{1}]`
        v: average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        rho: density :math:`\rho` of the medium in [M^{1} L^{-3}]
        fluid: fluid or gas of the medium
        geometry: geometry of the pipe
        retType: either 'headloss' or 'pressureloss'

    Kwargs Combinations:
        * f, l, D, v
        * f, l, D, v, rho
        * f, l, D, v, rho, retType
        * f, l, D, v, fluid
        * f, l, D, v, fluid, retType
        * f, geometry, v
        * f, geometry, v, rho
        * f, geometry, v, rho, retType
        * f, geometry, v, fluid
        * f, geometry, v, fluid, retType

    Returns:
        headloss :math:`h_l` in :math:`[L^{1}]`
    """
    if 'fluid' in kwargs.keys():
        fluid = kwargs.pop('fluid')
        if fluid.rho:
            kwargs['rho'] = fluid.rho
    if 'geometry' in kwargs:
        geometry = kwargs.pop('geometry')
        kwargs['l'] = geometry.l
        kwargs['D'] = geometry.D

    if 'retType' in kwargs.keys():
        retType = kwargs.pop('retType')
        if retType == 'headloss':
            return darcy_head_loss(**kwargs)
        elif retType == 'pressureloss':
            return darcy_pressure_loss(**kwargs)

    return function_switch(darcy_head_loss,
                           darcy_pressure_loss,
                           **kwargs)


def darcy_head_loss(f, l, D, v):
    r"""
    Calculate the headloss :math:`h_l` in :math:`[L^{1}]` using Darcy's equation:

    .. math::

       h_l = f \frac{l}{d} \frac{v^2}{2g}

    Args:
        f: darcy-weisbach friction factor :math:`f` in :math:`[-]`
        l: length :math:`l` of a pipe in :math:`[L^{1}]`
        d: diameter :math:`d` in :math:`[L^{1}]`
        v: average velocity :math:`v` in :math:`[L^{1} t^{-1}]`

    Returns:
        head loss :math:`h_l` in :math:`[L^{1}]`
    """
    return - f * l / D * v ** 2 / (2 * u.g_0)


def darcy_pressure_loss(f, l, D, v, rho):
    r"""
    Calculate the pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]` using Darcy's equation:

    .. math::

       \Delta p = f \frac{l}{d} \frac{v^2 \rho}{2}

    Args:
        f: darcy-weisbach friction factor :math:`f` in :math:`[-]`
        l: length :math:`l` of a pipe in :math:`[L^{1}]`
        D: diameter :math:`d` in :math:`[L^{1}]`
        v: average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        rho: density :math:`\rho` of the medium in [M^{1} L^{-3}]

    Returns:
         pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]`
    """
    return - f * l / D * kinetic_energy(v, rho)


def darcy_pressure_loss_from_kinetic_energy(f, l, D, e_k):
    r"""
    Calculate the pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]` using Darcy's equation:

    .. math::

       \Delta p = f \frac{l}{d} e_k

    Args:
        f: darcy-weisbach friction factor :math:`f` in :math:`[-]`
        l: length :math:`l` of a pipe in :math:`[L^{1}]`
        D: diameter :math:`d` in :math:`[L^{1}]`
        e_k: kinetic energy :math:`e_k` in :math:`[M^{1} L^{-1} t^{-2}]`

    Returns:
         pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]`
    """
    return - f * l / D * e_k


def pressure_loss_from_resistance_coefficient(K, v, rho):
    r"""
    Calculate the pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]` with a known resistance coefficient
    :math:`K`.

    .. math::

       \Delta p = K \frac{1}{2} \rho v^2

    Args:
        K: resistance coefficient :math:`K` in :math:`[-]`
        v: average velocity :math:`v` in :math:`[L^{1} t^{-1}]`
        rho: density :math:`\rho` of the medium in [M^{1} L^{-3}]

    Returns:
        pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]`
    """
    return - K * kinetic_energy(v, rho)


def pressure_loss_from_resistance_coefficient_and_kinetic_energy(K, e_k):
    r"""
    Calculate the pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]` with a known resistance coefficient
    :math:`K`.

    .. math::

       \Delta p = K e_k

    Args:
        K: resistance coefficient :math:`K` in :math:`[-]`
        e_k: kinetic energy :math:`e_k` in :math:`[M^{1} L^{-1} t^{-2}]`

    Returns:
        pressure loss :math:`\Delta p` in :math:`[M^{1} L^{-1} t^{-2}]`
    """
    return - K * e_k


def pressure_loss(**kwargs):
    if 'fluid' in kwargs:
        fluid = kwargs.pop('fluid')
        kwargs['rho'] = fluid.rho
    if 'geometry' in kwargs:
        geometry = kwargs.pop('geometry')
        kwargs['l'] = geometry.l
        kwargs['D'] = geometry.D
    return function_switch(darcy_pressure_loss,
                           darcy_pressure_loss_from_kinetic_energy,
                           pressure_loss_from_resistance_coefficient,
                           pressure_loss_from_resistance_coefficient_and_kinetic_energy,
                           **kwargs)


def Ar(**kwargs):
    r"""
    Archimedes number to determine the motion of fluids due to density differences, given in :math:`[-]`

    .. math::

        Ar = \frac{g L^3}{\nu^2} \frac{\rho_b - \rho_m}{\rho_m}

    or

    .. math::

        Ar = \frac{g L^3 \rho_m(\rho_b -\rho_m)}{\mu^2}

    Args:
        L: length :math:`L` of the body in :math:`[L^{1}]`
        rho_m: density :math:`\rho_m` of a medium in :math:`[M^{1} L^{-3}]`
        rho_b: density :math:`\rho_b` of a body in :math:`[M^{1} L^{-3}]`
        nu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`
        mu: dynamic viscosity :math:`\nu` in :math:`'[L^{2} t^{-1}]`
        medium: fluid in which the body resides

    Kwargs Combinations:
        * L, nu, rho_m, rho_b
        * L, rho_b, medium
        * L, body, medium
        * L, nu, rho_m, body
        * L, rho_b, rho_m, mu
        * L, mu, rho_m, body

    Returns:
        Archimedes number :math:`Ra` in :math:`[-]`
    """
    if 'medium' in kwargs.keys():
        medium = kwargs.pop('medium')
        if medium.nu:
            kwargs['nu'] = medium.nu
        elif medium.mu:
            kwargs['mu'] = medium.mu
        elif medium.rho:
            kwargs['rho_m'] = medium.rho

    if 'body' in kwargs.keys():
        body = kwargs.pop('body')
        if body.rho:
            kwargs['rho_b'] = body.rho

    return function_switch(Ar_kinematic_viscosity,
                           Ar_dynamic_viscosity,
                           **kwargs)


def Ar_kinematic_viscosity(L, rho_m, rho_b, nu):
    r"""
    Archimedes number to determine the motion of fluids due to density differences, given in :math:`[-]`

    .. math::

        Ar = \frac{g L^3}{\nu^2} \frac{\rho_b - \rho_m}{\rho_m}

    Args:
        L: length :math:`L` of the body in :math:`[L^{1}]`
        rho_m: density :math:`\rho_m` of a medium in :math:`[M^{1} L^{-3}]`
        rho_b: density :math:`\rho_b` of a body in :math:`[M^{1} L^{-3}]`
        nu: dynamic viscosity :math:`\mu` in :math:`[M^{1} L^{-1} t^{-1}]`

    Returns:
        Archimedes number :math:`Ra` in :math:`[-]`
    """

    return (u.g_0 * L ** 3) / nu ** 2 * (rho_b - rho_m) / rho_m


def Ar_dynamic_viscosity(L, rho_m, rho_b, mu):
    r"""
    Archimedes number to determine the motion of fluids due to density differences, given in :math:`[-]`

    .. math::

        Ar = \frac{g L^3 \rho_m(\rho_b -\rho_m)}{\mu^2}

    Args:
        L: length :math:`L` of the body in :math:`[L^{1}]`
        rho_m: density :math:`\rho_m` of a medium in :math:`[M^{1} L^{-3}]`
        rho_b: density :math:`\rho_b` of a body in :math:`[M^{1} L^{-3}]`
        mu: dynamic viscosity :math:`\nu` in :math:`'[L^{2} t^{-1}]`

    Returns:
        Archimedes number :math:`Ra` in :math:`[-]`
    """
    return (u.g_0 * L ** 3 * rho_m * (rho_b - rho_m)) / mu ** 2


def mass_flow(**kwargs):
    if 'fluid' in kwargs:
        fluid = kwargs.pop('fluid')
        kwargs['rho'] = fluid.rho
    return function_switch(mass_flow_from_speed,
                           mass_flow_from_volume_flow,
                           **kwargs)


def mass_flow_from_speed(v, A, rho):
    return v * A * rho


def mass_flow_from_volume_flow(V, rho):
    return V * rho


def kinetic_energy(v, rho):
    return 0.5 * v ** 2 * rho


def potential_energy(z, rho):
    return u.g_0 * z * rho


def bernoulli(**kwargs):
    r"""

    Args:
        **kwargs:

    Returns:

    """
    if 'fluid' in kwargs:
        fluid = kwargs.pop('fluid')
        kwargs['rho'] = fluid.rho
        kwargs['p'] = fluid.p
    p = kwargs.get('p', 0. * u.Pa)
    if 'e_k' in kwargs:
        e_k = kwargs['e_k']
    else:
        e_k = kinetic_energy(v=kwargs.get('v', 0. * u.m/u.s), rho=kwargs['rho'])
    if 'e_p' in kwargs:
        e_p = kwargs['e_p']
    else:
        e_p = potential_energy(z=kwargs.get('z', 0. * u.m), rho=kwargs['rho'])
    return e_k + e_p + p
