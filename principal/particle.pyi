from MTIpython.core.typing import t_amount, t_liquid, t_granular


def settling_velocity(rho_f: t_amount, mu: t_amount, d: t_amount, rho_s: t_amount) -> t_amount: ...


def settling_velocity(medium: t_liquid, granular: t_granular) -> t_amount: ...


def settling_velocity(medium: t_liquid, d: t_amount, rho_s: t_amount) -> t_amount: ...


def settling_velocity(rho_f: t_amount, mu: t_amount, granular: t_granular) -> t_amount: ...


def pickup_velocity(nu: t_amount, mu: t_amount, rho_f: t_amount, rho_s: t_amount, d: t_amount,
                    D: t_amount) -> t_amount: ...


def pickup_velocity(medium: t_liquid, granular: t_granular, D: t_amount) -> t_amount: ...


def pickup_velocity(nu: t_amount, mu: t_amount, rho_f: t_amount, granular: t_granular, D: t_amount) -> t_amount: ...


def pickup_velocity(medium: t_liquid, rho_s: t_amount, d: t_amount, D: t_amount) -> t_amount: ...
