from MTIpython.core.typing import t_amount, t_liquid


def pipe_diameter(epsilon: t_amount, L: t_amount, h_l: t_amount, nu: t_amount,
                  Q: t_amount) -> t_amount: ...


def pipe_diameter(epsilon: t_amount, L: t_amount, h_l: t_amount, fluid: t_liquid,
                  Q: t_amount) -> t_amount: ...
