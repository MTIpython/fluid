r"""
.. module:: MTIpython.fluid.principal
    :platform: Unix, Windows
    :synopsis: packages for fluid calculations

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>, Maarten in 't Veld <jm.intveld@ihcmti.com>,
    Edwin de Hoog <e.dehoog@ihcmti.com>
"""

from MTIpython.fluid.principal import core, particle, design

__all__ = ['core', 'particle', 'design']
