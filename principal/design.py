r"""
.. module:: MTIpython.fluid.principal.design
    :platform: Unix, Windows
    :synopsis: packages with commonly used fluid calculations for component design

.. moduleauthor:: Jelle Spijker <j.spijker@ihcmti.com>
"""
from MTIpython.core.functions import function_switch
from MTIpython.units.SI import u

__all__ = ['pipe_diameter']


def pipe_diameter(**kwargs):
    r"""
    Pipe design diameter

    .. math::

        d = 0.66 \left(\varepsilon^{1.25} \left(\frac{L Q^{2}}{g h_l}\right)^{4.75} + \nu Q^{9.4}
        \left(\frac{L}{g h_l}\right)^{5.2}\right)^{0.04}

    See :cite:`mott_toegepaste_2009`

    Args:
        epsilon: Surface roughness :math:`\varepsilon` of the pipe in :math:`[L^{1}]`
        L: Length :math:`L` of the pipe in :math:`[L^{1}]`
        h_l: Headloss :math:`h_l` in :math:`[L^{1}]`
        nu: Fluid kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
        Q: Volumetric flow :math:`V` through the pipe in :math:`[L^{3} t^{-1}]`
        fluid: medium flowing through the pipe

    Returns:
        d: :math:`d` in :math:`[L^{1}]`
    """
    if 'fluid' in kwargs.keys():
        fluid = kwargs.pop('fluid')
        kwargs['nu'] = fluid.nu
    return function_switch(pipe_diameter_cat3,
                           **kwargs)


def pipe_diameter_cat3(epsilon, L, h_l, nu, Q):
    r"""
    Pipe design diameter

    .. math::

        d = 0.66 \left(\varepsilon^{1.25} \left(\frac{L Q^{2}}{g h_l}\right)^{4.75} + \nu Q^{9.4}
        \left(\frac{L}{g h_l}\right)^{5.2}\right)^{0.04}

    See :cite:`mott_toegepaste_2009`

    Args:
        epsilon: Surface roughness :math:`\varepsilon` of the pipe in :math:`[L^{1}]`
        L: Length :math:`L` of the pipe in :math:`[L^{1}]`
        h_l: Headloss :math:`h_l` in :math:`[L^{1}]`
        nu: Fluid kinematic viscosity :math:`\nu` in :math:`[L^{2} t^{-1}]`
        Q: Volumetric flow :math:`V` through the pipe in :math:`[L^{3} t^{-1}]`

    Returns:
        d: :math:`d` in :math:`[L^{1}]`
    """

    @u.wraps('m', ('m', 'm', 'm', 'm**2/s', 'm**3/s'))
    def func(epsilon, L, h_l, nu, Q):
        g = 1. * u.g_0
        g = g.to('m/s**2').m
        d = 0.66 * (epsilon ** 1.25 * ((L * Q ** 2) / (g * h_l)) ** 4.75 + nu * Q ** 9.4 * (
                L / (g * h_l)) ** 5.2) ** 0.04
        return d

    d = func(epsilon, L, h_l, nu, Q)
    return d
